package maxdistructo.discord.bots.omega.listeners

import maxdistructo.discord.bots.omega.Omega
import net.dv8tion.jda.api.events.channel.category.CategoryCreateEvent
import net.dv8tion.jda.api.events.channel.category.CategoryDeleteEvent
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter

class LoggerListener : ListenerAdapter() {
    val log = Omega.LOGGER
    override fun onCategoryCreate(event: CategoryCreateEvent) {
        log.info("A Category (${event.category.name}) was created in ${event.guild.name}.")
    }
    override fun onCategoryDelete(event: CategoryDeleteEvent) {
        log.info("${event.category.name} was deleted from guild ${event.guild.name}")
    }

    override fun onGuildMessageReceived(event: GuildMessageReceivedEvent) {
        log.info("[${event.guild.name}] #${event.channel.name} (${event.author.name}#${event.author.discriminator}) " + event.message.contentDisplay)
    }

    override fun onPrivateMessageReceived(event: PrivateMessageReceivedEvent) {
        log.info("[DM] #${event.channel.name} (${event.author.name}#${event.author.discriminator}) " + event.message.contentDisplay)
    }

}