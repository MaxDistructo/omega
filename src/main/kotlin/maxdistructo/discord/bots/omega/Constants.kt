package maxdistructo.discord.bots.omega

import maxdistructo.discord.core.JSONUtils

object Constants {
    val token = JSONUtils.readJSONFromFile("/config.json").getString("token")!!
    const val ownerId = 228111371965956099L
    val prefix = JSONUtils.readJSONFromFile("/config.json").getString("prefix")!!
    const val BOT_NAME = "Omega"
    const val BOT_VERSION = "1.0_BETA"
}