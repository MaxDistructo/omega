package maxdistructo.discord.bots.omega.commands.casino.internal

import maxdistructo.discord.bots.omega.Omega
import maxdistructo.discord.core.JSONUtils
import net.dv8tion.jda.api.JDA
import org.json.JSONObject
import java.time.Instant

/**
 * CasinoPlayer
 * An object representation of a User's config file and translation of it.
 */

class CasinoPlayer(val id : Long, jda : JDA) {
    init{
        reload()
    }
    private var internalConfig = UserCasinoConfig(0)
    val player = jda.getUserById(id)
    private var internalChips : Long = 0
    private var internalBj = 0
    private var internalAllin = 0
    private var internalDailies = 0
    private var internalLastDaily = 0L
    private fun write(){
        Omega.LOGGER.debug("Writing " + this.toJSON().toString())
        JSONUtils.writeJSONToFile("/casino/$id.json", this.toJSON())
    }
    fun getLastDaily() : Long{
        return internalLastDaily
    }
    fun putBj(bj: Int){
        internalBj = bj
        write()
    }
    fun putLastDaily(){
        internalLastDaily = Instant.now().epochSecond
        write()
    }
    fun putAllin(allin : Int){
        internalAllin = allin
        write()
    }
    fun putDailies(dailies: Int){
        internalDailies = dailies
        write()
    }
    fun putChips(chips: Long){
        internalChips = chips
        write()
    }
    fun addChips(chips: Long){
        this.internalChips += chips
        write()
    }
    fun removeChips(chips: Long){
        internalChips -= chips
        write()
    }
    fun getRank() : String{
        return CasinoUtils.getRank(this.internalChips)
    }
    fun getBj(): Int{
        return internalBj
    }
    fun getAllin() : Int{
        return internalAllin
    }
    fun getDailies(): Int{
        return internalDailies
    }
    fun getChips() : Long{
        return internalChips
    }
    private fun reload(){
        this.internalConfig = UserCasinoConfig(id)
        this.internalChips = internalConfig.chips
        this.internalBj = internalConfig.gamePlays.getInt("bj")
        this.internalAllin = internalConfig.gamePlays.getInt("allin")
        this.internalDailies = internalConfig.gamePlays.getInt("dailies")
        this.internalLastDaily = internalConfig.dailies
    }
    private fun getConfig() : UserCasinoConfig{
        return UserCasinoConfig(id)
    }
    fun toJSON() : JSONObject{
        val json = JSONObject()
        json.put("chips", internalChips)
        json.put("id", player!!.idLong)
        json.put("gamePlays", JSONObject().put("bj", internalBj).put("allin", internalAllin).put("dailies", internalDailies))
        json.put("lastdaily", internalLastDaily)
        return json
    }
    override fun toString() : String{
        return this.toJSON().toString()
    }
}