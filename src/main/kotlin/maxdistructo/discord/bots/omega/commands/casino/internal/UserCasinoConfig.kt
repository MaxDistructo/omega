package maxdistructo.discord.bots.omega.commands.casino.internal

import maxdistructo.discord.core.JSONUtils
import org.json.JSONObject

class UserCasinoConfig(id : Long) {
        val json: JSONObject = try {
            val internal = JSONUtils.readJSONFromFile("/casino/$id.json")
            internal.get("chips")
            internal
        }
        catch(e : Exception){
            JSONUtils.writeJSONToFile("/casino/$id.json", JSONObject().put("chips", 0).put("gamePlays", JSONObject().put("bj",0).put("allin",0).put("dailies",0)).put("id",id).put("lastdaily", 0L))
            JSONObject().put("chips", 0).put("gamePlays", JSONObject().put("bj",0).put("allin",0).put("dailies",0)).put("id",id).put("lastdaily", 0L)
        }
        val gamePlays: JSONObject= json.getJSONObject("gamePlays")
        val chips = json.getLong("chips")
        val dailies = json.getLong("lastdaily")
}