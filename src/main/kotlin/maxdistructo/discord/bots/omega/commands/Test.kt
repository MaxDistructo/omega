package maxdistructo.discord.bots.omega.commands

import com.jagrosh.jdautilities.command.Command
import com.jagrosh.jdautilities.command.CommandEvent

class Test : Command() {
    init{
        this.name = "test"
        this.guildOnly = false
        this.help = "Test command"
    }

    override fun execute(event: CommandEvent?) {
        event!!.reply("Test")
    }
}