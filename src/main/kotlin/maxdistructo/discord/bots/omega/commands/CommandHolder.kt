package maxdistructo.discord.bots.omega.commands

import com.jagrosh.jdautilities.command.Command

abstract class CommandHolder {
    abstract val name : String
    abstract val commands : Collection<Command>
}