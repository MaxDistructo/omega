package maxdistructo.discord.bots.omega.commands.casino.internal

import maxdistructo.discord.core.jda.UnicodeEmoji
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed

object CasinoUtils {
    fun getRank(chips: Long) : String {
        return when{
            chips > 1000000 -> UnicodeEmoji.STAR
            chips > 500000 -> "King"
            chips > 250000 -> "Queen"
            chips > 125000 -> "Jack"
            chips > 62500 -> "A"
            chips > 31250 -> "10"
            chips > 15625 -> "9"
            chips > 7812 -> "8"
            chips > 3906 -> "7"
            chips > 1953 -> "6"
            chips > 976 -> "5"
            chips > 488 -> "4"
            chips > 244 -> "3"
            chips > 122 -> "2"
            else -> "None"
        }
    }
    fun getInfoEmbed(casinoPlayer: CasinoPlayer): MessageEmbed {
        val builder = EmbedBuilder()
        builder.setAuthor(casinoPlayer.player!!.name)
        builder.setTitle("Casino Information")
        builder.setImage(casinoPlayer.player.avatarUrl)
        builder.setDescription("Casino Rank: ${casinoPlayer.getRank()} \n Chip Balance: " + casinoPlayer.getChips() + "\n Allin Play Count: ${casinoPlayer.getAllin()} \n Blackjack Play Count: ${casinoPlayer.getBj()} \n Daily Count: ${casinoPlayer.getDailies()}")
        return builder.build()
    }
}
