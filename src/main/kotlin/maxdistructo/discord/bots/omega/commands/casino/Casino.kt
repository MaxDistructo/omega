package maxdistructo.discord.bots.omega.commands.casino

import com.jagrosh.jdautilities.command.Command
import com.jagrosh.jdautilities.command.CommandEvent
import maxdistructo.discord.bots.omega.Omega
import maxdistructo.discord.bots.omega.commands.CommandHolder
import maxdistructo.discord.bots.omega.commands.casino.internal.CasinoPlayer
import maxdistructo.discord.bots.omega.commands.casino.internal.CasinoUtils
import maxdistructo.discord.core.Utils
import maxdistructo.discord.core.jda.JDAUtils
import maxdistructo.discord.core.jda.message.Messages
import java.time.Instant
import kotlin.math.roundToLong
import kotlin.random.Random

object Casino {
    class Commands: CommandHolder(){
        override val name: String
            get() = "Casino"
        override val commands: Collection<Command>
            get() = listOf(CasinoCommand())
    }
    class CasinoCommand: Command(){
        init{
            this.name = "casino"
            this.guildOnly = true
            this.help = "Gets your casino info."
            this.children = arrayOf(InfoCommand(), DailyCommand())
            this.hidden = true
        }

        override fun execute(event: CommandEvent) {
            Omega.LOGGER.debug("Executing Casino")
            val player = CasinoPlayer(Utils.convertToLong(event.author.id)!!,event.jda)
            event.reply(CasinoUtils.getInfoEmbed(player))
        }
    }
    class InfoCommand: Command(){
        init{
            this.name = "info"
            this.guildOnly = true
            this.help = "Gets a players casino information"
            Omega.LOGGER.debug("Info Command Built")
        }
        override fun execute(event: CommandEvent) {
            Omega.LOGGER.debug("Executing Casino#Info")
            val splitMessage = event.message.contentDisplay.split(" ")
            val player = CasinoPlayer(JDAUtils.getUserFromInput(event.message, splitMessage[2])!!.idLong, event.jda)
            event.reply(CasinoUtils.getInfoEmbed(player))
        }
    }
    class DailyCommand: Command(){
        init{
            this.name = "daily"
            this.guildOnly = true
            this.help = "Get your daily!"
            Omega.LOGGER.debug("Daily Command Built")
        }

        override fun execute(event: CommandEvent?) {
            Omega.LOGGER.debug("Executing Casino#Daily")
            val player = CasinoPlayer(Utils.convertToLong(event!!.author.id)!!, event.jda)
            val dailyDifference = Instant.now().epochSecond - player.getLastDaily()
            if(dailyDifference >= 86400){
                player.addChips(50)
                player.putLastDaily()
                event.reply("You have received your daily 50 chips")
            }
            else{
                event.reply("Your daily is not available at this time.")
            }
        }
    }
    class AllinCommand: Command(){
        init{
            this.name = "allin"
            this.guildOnly = true
            this.help = "Put in all your chips to hopefully get a bonus"
        }

        override fun execute(event: CommandEvent?) {
            Omega.LOGGER.debug("Executing Allin")
            val player = CasinoPlayer(event!!.author.idLong, event.jda)
            val message = Messages.sendMessage(event.channel, "${event.member.effectiveName}, you will be betting ${player.getChips()} for a chance to gain up to 125% of what you bet.")
            message.editMessage("Rolling the dice. Please wait.")
            Thread.sleep(300)
            val random = Random.nextInt(0,75)
            if(random <= 25){ // 1/3 of the time, give the 1.25% of the current chip count.
                val earnings : Long = (player.getChips() * .25).roundToLong()
                player.putChips((player.getChips() + earnings))
                message.editMessage("You WIN! You have earned $earnings!")
            }
            else{
                player.putChips(0)
                message.editMessage("You lose. You have lost all your chips.")
            }
        }
    }
    class BlackjackCommand: Command(){
        init{
            this.name = "blackjack"
            this.guildOnly = true
            this.help = "Play Blackjack to earn some chips."
        }

        override fun execute(event: CommandEvent?) {

        }
    }

}