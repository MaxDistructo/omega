package maxdistructo.discord.bots.omega

import ch.qos.logback.classic.Logger
import com.jagrosh.jdautilities.command.CommandClientBuilder
import com.jagrosh.jdautilities.examples.command.PingCommand
import com.jagrosh.jdautilities.examples.command.ShutdownCommand
import maxdistructo.discord.bots.omega.commands.CommandHolder
import maxdistructo.discord.bots.omega.commands.Test
import maxdistructo.discord.bots.omega.commands.casino.Casino
import maxdistructo.discord.bots.omega.listeners.LoggerListener
import maxdistructo.discord.core.Utils
import maxdistructo.discord.core.jda.impl.Bot
import org.reflections.Reflections
import org.slf4j.LoggerFactory
import java.io.File
import java.net.URLClassLoader

/**
 * @object Omega
 * Main class of the bot. Handles most of the basic setup of the bot.
 */
object Omega {
    val LOGGER = LoggerFactory.getLogger(this.javaClass) as Logger
    @JvmStatic
    fun main (args : Array<String>){
        val bot = Bot(Constants.token)
        bot.setOwnerId(Constants.ownerId) //Set my ID as a static Owner ID
        val builder = CommandClientBuilder()
        builder.useDefaultGame()
        builder.setOwnerId(Constants.ownerId.toString())
        builder.setEmojis("\uD83D\uDE03", "\uD83D\uDE2E", "\uD83D\uDE26")
        builder.setPrefix(Constants.prefix)
        /*
        val reflection = Reflections("maxdistructo.discord.bots.omega")
        val classes = reflection.getSubTypesOf(CommandHolder::class.java)
        for(c in classes){
            LOGGER.debug("Adding ${c.name}'s commands to " + Constants.BOT_NAME)
            for(value in c.newInstance().commands){
                builder.addCommand(value)
            }
        }
        */
        builder.addCommands(PingCommand(), ShutdownCommand(), Test(), Casino.CasinoCommand())
        val commandListener = builder.build()
        bot.init()
        bot.client.addEventListener(commandListener, LoggerListener())
    }
}