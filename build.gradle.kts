import org.jetbrains.kotlin.gradle.dsl.KotlinJvmOptions
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

buildscript {
	repositories {
		mavenCentral()
	}
	dependencies {
		classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.3.41")
	}
}
plugins {
	java
	idea
	`java-library`
	maven

	kotlin("jvm") version "1.3.41"
}
group = "com.gitlab.MaxDistructo"

repositories {
	jcenter()
	mavenCentral()
	maven {
		name = "Jitpack"
		url = uri("https://jitpack.io")
	}
	maven{
		name = "JDA Bintray"
		url = uri("https://dl.bintray.com/dv8fromtheworld/maven")
	}
}

// In this section you declare the dependencies for your production and test code
dependencies {
	implementation (group = "com.gitlab.MaxDistructo", name = "mdCore-JDA", version = "1.7.0")
	compile (group = "com.jagrosh", name = "jda-utilities", version = "3.0.1")
	compile (group = "org.jetbrains.kotlin", name = "kotlin-stdlib-jdk8")
	compile (group = "com.mashape.unirest", name = "unirest-java", version = "1.4.9")
	compile (group = "org.reflections", name = "reflections", version = "0.9.11")
}

tasks {
	withType<KotlinCompile> {
		(kotlinOptions).apply {
			jvmTarget = JavaVersion.VERSION_1_8.toString()
		}
	}
	val copyToLib by registering(Copy::class) {
		into("$buildDir/lib")
		from(configurations.compile)
	}
	val stage by registering {
		dependsOn("build", copyToLib)
	}
}
